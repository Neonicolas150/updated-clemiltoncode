<?php
    require_once('../Conexao/Conexao.class.php');
    require_once('../modelo/userModel.php');
    try {
        $conn = new Conexao("../Conexao/configDB.ini");
        $pdo = $conn->getPDO();
        
        $userNome = $_GET['userNome'];

        $comando = $pdo->prepare('DELETE FROM userinfo WHERE userNome=:n');
        $comando->bindValue(":n", $userNome);
        if($comando->execute()) {
            header("refresh:0, ../../frontend/userLista.php");
            echo '<script>alertify.alert("ready")';
        } else {
            echo "A conta do usuário ".$userNome." não foi excluída devido a problemas inesperados.";
        }


    } catch(PDOExpection $e) {
        echo "Surgiu um erro inesperado relacionado ao Banco de Dados: ".$e->getMessage();
    }



?>